﻿<?php
/******************************************************************************/
/*                                                                            */
/* Titre          : Vérifier les balises META d'un site Internet              */
/*                                                                            */
/* Description    : Permet de vérifier la présence et de visualiser           */
/*                  directement toutes les balises META de l'ensemble         */
/*                  des pages d'un site Internet.                             */
/* URL            : http://www.nicolasparis.com/                              */
/* Auteur         : Nicolas PARIS                                             */
/* Date édition   : 13 Avril 2015                                             */
/* Version        : 1.0                                                       */
/*                                                                            */
/******************************************************************************/

$fichier = "sitemap.xml";
$urlSiteWeb = "http://www.exemple.com/";

$urlsTrouvees = array();
$metaKeys = array();

$dom = new DOMDocument;
$dom->load($urlSiteWeb . $fichier);
$elements = $dom->getElementsByTagName("loc");

foreach($elements as $element) {
	$urlsTrouvees[] = $element->nodeValue;
	
	// on verifie que les urls trouvées sont bien des pages web (et non des documents)
	$headers = get_headers($element->nodeValue, 1);
	if ($headers['Content-Type'] == "text/html" or $headers['Content-Type'] == "text/html; charset=utf-8") {
		echo "- URL : " . $element->nodeValue . "<br>";

		preg_match("#<title>(.*)</title>#Ui", file_get_contents($element->nodeValue), $titre);
		echo "-- Title : " . $titre[1] . "<br>";

		// on va verifier la présence de certaines balises meta pour chaque url de page web
		$metaTags = get_meta_tags($element->nodeValue);

		foreach ($metaTags as $key => $value) {
			$metaKeys[] = $key;
		}

		if (!in_array('description', $metaKeys)) {
			echo "il y n'a pas de balise meta description !<br>";
		} else {
			echo "-- Description : " . $metaTags['description'] . "<br>";
		}
		
		if (!in_array('keywords', $metaKeys)) {
			echo "il y n'a pas de balise meta keywords !<br>";
		} else {
			echo "-- Keywords : " . $metaTags['keywords'] . "<br>";
		}

		if (!in_array('robots', $metaKeys)) {
			echo "il y n'a pas de balise meta robots !<br>";
		} else {
			echo "-- Robots : " . $metaTags['robots'] . "<br>";
		}
	}

	echo "<br>";
}