# Meta Analyzer #

Permet de vérifier la présence et de visualiser les balises Meta (utiles pour le SEO) de l'ensemble des pages d'un site Internet.

### Installation ###

Ce script peut-être installé sur tout type de serveur LAMP (avec une version de PHP > 5).
Il suffit simplement de déposer le script sur le serveur (local ou distant), de définir le nom de domaine que vous souhaitez analyser dans la variable "$urlSiteWeb" et d'executer le script.

### Comment ça marche ? ###

Le script se base sur les informations présentent dans le fichier sitemap.xml de votre site Internet.

Une fois le fichier sitemap.xml trouvé, il parcours chacune des URLs de pages web trouvées et vérifit que les principales balises Méta (*title, description, keywords, robots*) soient bien présentent.
Si ce n'est pas le cas, il affichera une alerte sinon il affichera le contenu des balises.